package com.situ.lashoujsp.servlet;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by niugao on 10/24/2016.
 */
public class LoginServlet extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws IOException {

        //request.setCharacterEncoding("UTF-8");

        //ServletOutputStream out = response.getOutputStream();
        //response.setHeader("content-type","text/html;charset=UTF-8");
        //response.setContentType("text/html;charset=utf-8");
        //先设置输出编码
        response.setCharacterEncoding("UTF-8");
        //再取得输出对象
        PrintWriter out = response.getWriter();

        String html = createHtmlString(null,null);

        out.print(html);
    }

    @Override
    public void doPost(HttpServletRequest request,
                      HttpServletResponse response) throws IOException {


        String errmsg=null;
        String userName=null;

        if(request.getMethod().equals("POST")) {
            //验证用户名和密码
            userName = request.getParameter("userName");
            String password = request.getParameter("password");

            if (userName.equals("admin") && password.equals("admin")) {
                //登录成功
                response.sendRedirect("home.jsp");
                return;
            } else {
                //登录不成功，提示一下
                errmsg = "用户名和密码验证失败";
            }
        }


        //request.setCharacterEncoding("UTF-8");

        //ServletOutputStream out = response.getOutputStream();
        //response.setHeader("content-type","text/html;charset=UTF-8");
        //response.setContentType("text/html;charset=utf-8");
        //先设置输出编码
        response.setCharacterEncoding("UTF-8");
        //再取得输出对象
        PrintWriter out = response.getWriter();


        String html = createHtmlString(errmsg,userName);

        out.print(html);
    }

    String createHtmlString(String errmsg,String userName){
        String html ="<html>\n"+
                "<head>\n"+
                "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\" />\n"+
                "<title>Title</title>\n"+
                "<link rel=\"stylesheet\" href=\"bootstrap/css/bootstrap.min.css\">\n"+
                "<script src=\"jquery.min.js\"></script>\n"+
                "<script src=\"bootstrap/js/bootstrap.min.js\"></script>\n"+
                "</head>\n"+
                "<body>\n"+
                "<div class='container' style=\"margin-top: 100px;width: 500px;\">\n"+
                "<div class=\"panel panel-default\">\n"+
                "<div class=\"panel-heading\">\n"+
                "<h3 class=\"panel-title\">先登录</h3>\n"+
                "</div>\n"+
                "<div class=\"panel-body\" style=\"padding-left: 40px;padding-right: 40px;\">\n"+
                "<form method=\"post\" class=\"form-horizontal\">\n"+
                "<div class=\"form-group\">\n";
        if(userName!=null){
            html+="<input type=\"text\" class=\"form-control\" name=\"userName\" placeholder=\"用户名\" value='"+userName+"'>\n";
        }else{
            html+="<input type=\"text\" class=\"form-control\" name=\"userName\" placeholder=\"用户名\">\n";
        }
        html+="</div>\n"+
                "<div class=\"form-group\">\n"+
                "<input type=\"password\" class=\"form-control\" name=\"password\" placeholder=\"密码\">"+
                "</div>"+
                "<div class=\"form-group\">"+
                "<div class=\"checkbox\">"+
                "<label>"+
                "<input type=\"checkbox\"> 记住此帐户"+
                "</label>"+
                "</div>"+
                "</div>"+
                "<div class=\"form-group\">"+
                "<button type=\"submit\" class=\"btn btn-default\" style=\"width:300px;\">登录</button>"+
                "</div>";
        if(errmsg!=null){
            html+="<span class=\"label label-warning\" style=\"font-size: 1em;\">";
            html+=errmsg;
            html+="</span>";
        }
        html+="</form>"+
                "</div>"+
                "</div>"+
                "</div>"+
                "</body>"+
                "</html>";
        return html;
    }
}
