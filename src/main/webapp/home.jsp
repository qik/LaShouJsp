<%--
  Created by IntelliJ IDEA.
  User: niugao
  Date: 10/24/2016
  Time: 9:27 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>

    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <style>
        .aheight {
            padding-top: 0px;
            padding-bottom: 0px;
        }

        .ver_algin {
            float: none;
            display: table-cell;
            vertical-align: middle;
        }

        .d1 {
            margin-top: 10px;
        }

        .d1 a {
            color: #3c3c3c;
        }

        .menu_border {
            border-top: solid 2px chocolate;
            border-left: solid 2px chocolate;
            border-right: solid 2px chocolate;
        }

        #mavbar2 {
            margin-top: 20px;
            background-color: #985f0d;
            width: 100%;
            display: table;
        }

        #mavbar2 div {
            display: inline-block;
            margin-right: 20px;
        }

        .tuijian_box {
            padding: 10px;
        }

        .tuijian_box img {
            width: 100%;
        }

        .col_padding {
            margin: 5px;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-default" role="navigation" style="min-height: 0px;">
    <div class="container-fluid">
        <div>
            <ul class="nav navbar-nav">
                <li><a style="padding: 4px 10px;" href="#">收藏拉手网</a></li>
                <li>|</li>
                <li><a style="padding: 4px 10px;" href="#">帮照妖镜</a></li>
                <li style="color: #AcAcAc;">|</li>
                <li><a href="#" style="padding: 4px 10px;">反馈</a></li>
                <li>|</li>
            </ul>

            <button type="button" class="btn btn-primary navbar-btn navbar-right"
                    style="margin: 0px 5px; padding: 2px 15px;">
                登录
            </button>
            <button type="button" class="btn btn-warning navbar-btn navbar-right"
                    style="margin: 0px 5px; padding: 2px 15px;">
                注册
            </button>

            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a style="padding: 4px 10px;" href="#">
                        <span class="glyphicon glyphicon-shopping-cart"></span>
                        &nbsp;购物车(0)
                    </a>
                </li>
                <li class="dropdown">
                    <a style="padding: 4px 10px;" href="#" class="dropdown-toggle" data-toggle="dropdown">
                        最近浏览
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#">jmeter</a></li>
                        <li><a href="#">EJB</a></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a style="padding: 4px 10px;" href="#" class="dropdown-toggle" data-toggle="dropdown">
                        我的拉手
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#">我的订单</a></li>
                        <li><a href="#">我的收藏</a></li>
                        <li><a href="#">常用地址</a></li>
                        <li><a href="#">拉手帐本</a></li>
                        <li><a href="#">帐户设置</a></li>
                        <li><a href="#">我的消息</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3" style="display:table;">
            <img src="img/logo.png" class="ver_algin"/>
            <div class="ver_algin">
                <div>青岛</div>
                <div>
                    <a href="#">
                        <small>
                            切换城市
                            <span class="glyphicon glyphicon-chevron-down"></span>
                        </small>
                    </a>
                </div>
            </div>
        </div>

        <div class="col-xs-6">
            <div class="input-group">
                <input type="text" class="form-control"
                       placeholder="请输入商品、商家、商圈"
                       aria-describedby="basic-addon2">
                <span class="input-group-addon" id="basic-addon2">搜索</span>
            </div>
            <div class="d1">
                <a href="#">婚纱照</a>&nbsp;&nbsp;&nbsp;
                <a href="#">旅游</a>&nbsp;&nbsp;&nbsp;
                <a href="#">自助餐</a>&nbsp;&nbsp;&nbsp;
                <a href="#">蛋糕</a>&nbsp;&nbsp;&nbsp;
                <a href="#">ktv</a>&nbsp;&nbsp;&nbsp;
                <a href="#">美食</a>&nbsp;&nbsp;&nbsp;
                <a href="#">眼镜</a>&nbsp;&nbsp;&nbsp;
                <a href="#">美发</a>&nbsp;&nbsp;&nbsp;
                <a href="#">体检</a>
            </div>
        </div>
        <div class="col-xs-3">
            <img src="img/commitment.png"/>
        </div>
    </div>
</div>

<!--第二个导航栏-->
<div id="mavbar2">
    <div class="dropdown">
        <a href="#" class="dropdown-toggle menu_border" data-toggle="dropdown"
           style="font: 700 16px/40px Microsoft Yahei;background-color:white;padding: 5px 50px;margin-left: 30px;">
            查看全部分类
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="#">jmeter</a></li>
            <li><a href="#">EJB</a></li>
        </ul>
    </div>

    <div class="btn-group" role="group" aria-label="...">
        <button type="button" class="btn btn-default">Left</button>
        <button type="button" class="btn btn-default">Middle</button>
        <button type="button" class="btn btn-default">Right</button>
    </div>

    <div>
        <a href="#" style="color: white;font: 700 16px/40px Microsoft Yahei;">
            <span class="glyphicon glyphicon-shopping-cart"></span>
            &nbsp;购物车(0)
        </a>
    </div>

    <div class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown"
           style="color: white;font: 700 16px/40px Microsoft Yahei;">
            我的拉手
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="#">我的订单</a></li>
            <li><a href="#">我的收藏</a></li>
            <li><a href="#">常用地址</a></li>
            <li><a href="#">拉手帐本</a></li>
            <li><a href="#">帐户设置</a></li>
            <li><a href="#">我的消息</a></li>
        </ul>
    </div>
</div>

<!--分类区-->
<div class="panel panel-default" style="margin: 15px;">
    <div class="panel-heading">
        热　门：
        新单13 电影 自助餐 经济型酒店 门票 休闲零食 地方特产 火锅 KTV 周边游 养生休闲 热卖专场 话费充值
    </div>
    <div class="panel-body">
        分　类：
        全部 美食274 休闲娱乐224 电影9 生活服务1298 摄影写真800 酒店1462 旅游94 丽人192 教育培训64 购物1179
    </div>
    <div style="border-top:solid 1px silver;padding: 10px;">
        <div class="btn-group btn-group-sm" role="group" aria-label="...">
            <button type="button" class="btn btn-default">默认排序</button>
            <button type="button" class="btn btn-default">销量</button>
            <button type="button" class="btn btn-default">价格</button>
            <button type="button" class="btn btn-default">价格</button>
            <button type="button" class="btn btn-default">发布时间</button>
        </div>
    </div>
</div>

<!--产品展示区-->
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-8">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-6 col-sm-4 col-md-4">
                        <div class="col_padding">
                            <img style="width: 100%;" src="img/product/4.jpg"/>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-4">
                        <div class="col_padding">
                            <img style="width: 100%;" src="img/product/5.jpg"/>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-4">
                        <div class="col_padding">
                            <img style="width: 100%;" src="img/product/6.jpg"/>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-4">
                        <div class="col_padding">
                            <img style="width: 100%;" src="img/product/7.jpg"/>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-4">
                        <div class="col_padding">
                            <img style="width: 100%;" src="img/product/8.jpg"/>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-4">
                        <div class="col_padding">
                            <img style="width: 100%;" src="img/product/9.jpg"/>
                        </div>
                    </div>
                </div>
                <div class="row">

                </div>
                <div class="row">
                    <div class="col-xs-12" style="text-align: center;">
                        <ul class="pagination">
                            <li><a href="#">上一页</a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">下一页</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    销量排行
                </div>
                <div class="panel-body">
                    <div class="tuijian_box">
                        <img src="img/product/1.jpg"/>
                        <div>【257店通用】瑞康体检</div>
                        <div>瑞康体检贵宾套组</div>
                        <div>
                            <div style="display: inline-block;">¥699&nbsp;&nbsp;&nbsp;¥1179</div>
                            <div style="float:right;">已售923</div>
                        </div>
                    </div>
                    <div class="tuijian_box">
                        <img src="img/product/2.jpg"/>
                    </div>
                    <div class="tuijian_box">
                        <img src="img/product/3.jpg"/>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <ul id="myTab" class="nav nav-tabs">
                <li class="active">
                    <a href="#home" data-toggle="tab">W3Cschool Home</a>
                </li>
                <li><a href="#ios" data-toggle="tab">iOS</a></li>
                <li class="dropdown">
                    <a href="#" id="myTabDrop1" class="dropdown-toggle"
                       data-toggle="dropdown">Java <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="myTabDrop1">
                        <li><a href="#jmeter" tabindex="-1" data-toggle="tab">
                            jmeter</a>
                        </li>
                        <li><a href="#ejb" tabindex="-1" data-toggle="tab">
                            ejb</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <div id="myTabContent" class="tab-content">
                <div class="tab-pane fade in active "
                     style="border-left: solid 1px silver;border-right: solid 1px silver;border-bottom: solid 1px silver;"
                     id="home">
                    <p>W3Cschoool菜鸟教程是一个提供最新的web技术站点，本站免费提供了建站相关的技术文档，帮助广大web技术爱好者快速入门并建立自己的网站。菜鸟先飞早入行——学的不仅是技术，更是梦想。</p>
                </div>

                <div class="tab-pane fade"
                     style="border-left: solid 1px silver;border-right: solid 1px silver;border-bottom: solid 1px silver;"
                     id="ios">
                    <p>iOS 是一个由苹果公司开发和发布的手机操作系统。最初是于 2007 年首次发布 iPhone、iPod Touch 和 Apple
                        TV。iOS 派生自 OS X，它们共享 Darwin 基础。OS X 操作系统是用在苹果电脑上，iOS 是苹果的移动版本。</p>
                </div>

                <div class="tab-pane fade" id="jmeter">
                    <p>jMeter 是一款开源的测试软件。它是 100% 纯 Java 应用程序，用于负载和性能测试。</p>
                </div>

                <div class="tab-pane fade" id="ejb">
                    <p>Enterprise Java Beans（EJB）是一个创建高度可扩展性和强大企业级应用程序的开发架构，部署在兼容应用程序服务器（比如 JBOSS、Web Logic 等）的 J2EE 上。
                    </p>
                </div>
            </div>
            <script>
                $(function () {
                    $('#myTab li:eq(1) a').tab('show');
                });
            </script>
        </div>
    </div>
</div>

<!--其它信息-->
<div class="container-fluid"
     style="margin: 30px 0px; padding: 5px 15px;border-top:solid 1px gainsboro; ">
    <div class="row">
        <div class="col-xs-4">
            服务保障
            团购三包
            退换货服务
            支付方式
        </div>
        <div class="col-xs-4" style="border-left:solid 1px gainsboro;border-right:solid 1px gainsboro; ">
            用户帮助
            答疑反馈
            常见问题
            往期团购
            抽奖活动
        </div>
        <div class="col-xs-4">
            商务合作
            团购合作
            开放API
            拉手网联盟
            友情链接
        </div>
    </div>
</div>

</body>
</html>