<%--
  Created by IntelliJ IDEA.
  User: niugao
  Date: 10/24/2016
  Time: 9:27 AM
  To change this template use File | Settings | File Templates.
  登录页面
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String errmsg=null;

    if(request.getMethod().equals("POST")) {
        //验证用户名和密码
        String userName = request.getParameter("userName");
        String password = request.getParameter("password");

        if (userName.equals("admin") && password.equals("admin")) {
            //登录成功
            response.sendRedirect("home.jsp");
            return;
        } else {
            //登录不成功，提示一下
            errmsg = "用户名和密码验证失败";
        }
    }
%>

<html>
<head>
    <title>Title</title>

    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <script src="jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>

</head>
<body>

<div class="container" style="margin-top: 100px;width: 500px;">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">
                先登录！
            </h3>
        </div>
        <div class="panel-body" style="padding-left: 40px;padding-right: 40px;">
            <form method="post" class="form-horizontal">
                <div class="form-group">
                    <input type="text" class="form-control" name="userName" placeholder="用户名">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="password" placeholder="密码">
                </div>
                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox"> 记住此帐户
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-default" style="width:300px;">登录</button>
                </div>
                <% if(errmsg!=null){ %>
                <span class="label label-warning" style="font-size: 1em;">
                    <%=errmsg %>
                </span>
                <% } %>
            </form>
        </div>
    </div>
</div>

</body>
</html>
